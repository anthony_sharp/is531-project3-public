Prerequisites
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- [AWS CDK](https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html)
- Docker
- Python
- [jq](https://stedolan.github.io/jq/download/) 

Deployment 
1. Set up credentials - [AWS Docs](https://docs.aws.amazon.com/cdk/latest/guide/getting\_started.html)
2. Set up virtual env
    ```python3 -m venv .venv```\
    ```source .venv/bin/activate```
    ```pip install -r requirements-cdk.txt```\

4. Deploy application\
    ```make deploy-init```
5. Update deployment\
    ```make deploy```

6. Delete deployment\
    ```make destroy-all```

- Starting Frontend Dev Environment
    - Navigate to ~/flask_dev_env
    - Run: ```flask run --port=80 --host=0.0.0.0```
    - Navigate to http://0.0.0.0:80/ in the browser

- Potential Errors
    - Bootstrap
    ```cdk bootstrap```
