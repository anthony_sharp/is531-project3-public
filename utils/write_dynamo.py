import os
import boto3
import json

profile = os.getenv('AWS_PROFILE') if  'AWS_PROFILE' in os.environ else 'default'
session = boto3.Session(profile_name=profile)
s3 = session.client('s3')
ddb = session.client('dynamodb')

bucket='is-531-project-3-static-assets-123456'
ddb_table='is-531-project-3-dynamodb-table-12345'


def get_json():
    f = open('utils/national_parks.json')
    parks = json.load(f)
    f.close()
    return parks

def get_s3_urls():
    urls = []
    response = s3.list_objects_v2(
        Bucket=bucket,
        MaxKeys=123,
        Prefix='national_parks',
    )

    for img in response['Contents']:
        url = f'https://{bucket}.s3.amazonaws.com/{img["Key"]}'
        urls.append(url)
    return urls

def match_park_to_url(parks):
    for park in parks:
        name_list = park.split(' ')

        for url in urls:
            match = True
            for name in name_list:
                if name.lower() not in url:
                    match = False
            if match:
                parks[park]['url'] = url

    return parks

def write_to_dynamo(parks):
    for park, info in parks.items():
        ddb.put_item(
            Item={
                'park': {
                    'S': park,
                },
                'location': {
                    'S': info['location'],
                },
                'url': {
                    'S': info['url'],
                },
            },
            TableName=ddb_table,
        )



### MAIN ###
parks = get_json()
urls = get_s3_urls()
parks = match_park_to_url(parks)

# print(parks)
write_to_dynamo(parks)




