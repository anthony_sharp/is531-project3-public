from flask import Flask, render_template
from boto3.dynamodb.conditions import Key
import boto3
import os
import json


app = Flask(__name__)

session = boto3.Session()
ddb = session.client(service_name='dynamodb', region_name='us-east-1')

ddb_table='is-531-project-3-dynamodb-table-12345'

parks = {'Items':
    [
        {
            'url':
                {
                    'S':'https://i.natgeofe.com/n/40c8ec76-10ca-483d-9a78-aa7d75e12150/zion-national-park-canyon-junction-bridge.jpg?w=636&h=424'
                },
            'location':
                {
                    'S':'Utah'
                },
            'park':
                {
                    'S':'Zion'
                }
        },
        {
            'url':
                {
                    'S':'https://i.natgeofe.com/n/bb6c6e87-712f-4668-8765-3c607a813597/yellowstone-national-park-great-fountain-geyser.jpg?w=636&h=424'
                },
            'location':
                {
                    'S':'Wyoming'
                },
            'park':
                {
                    'S':'Yellowstone'
                }
        },
        {
            'url':
                {
                    'S':'https://i.natgeofe.com/n/d059c3be-4f7b-4115-aa99-1c9896605969/acadia-national-park-bass-harbor-lighthouse.jpg?w=636&h=424'
                },
            'location':
                {
                    'S':'Maine'
                },
            'park':
                {
                    'S':'Acadia'
                }
        },
        {
            'url':
                {
                    'S':'https://i.natgeofe.com/n/5cde8852-c0c5-4bd0-b1cc-a16fb37648a8/yosemite-falls-moonbow-yosemite-national-park.jpg?w=636&h=424'
                },
            'location':
                {
                    'S':'California'
                },
            'park':
                {
                    'S':'Yosmite'
                }
        },
        {
            'url':
                {
                    'S':'https://i.natgeofe.com/n/38bd1886-861d-40f2-9a28-ca48263bf315/giant-tree-trail-sequoia-national-park.jpg?w=636&h=424'
                },
            'location':
                {
                    'S':'California'
                },
            'park':
                {
                    'S':'Sequoia'
                }
        },
        {
            'url':
                {
                    'S':'https://i.natgeofe.com/n/3531ab0d-9b14-4762-ad98-87048b8b7d6b/crater-lake-national-park.jpg?w=636&h=424'
                },
            'location':
                {
                    'S':'Oregon'
                },
            'park':
                {
                    'S':'Crater Lake'
                }
        },
        {
            'url':
                {
                    'S':'https://i.natgeofe.com/n/84890bce-f204-4574-b753-408e2aca879d/grand-canyon-national-park-color-DSF4843.jpg?w=636&h=424'
                },
            'location':
                {
                    'S':'Arizona'
                },
            'park':
                {
                    'S':'Grand Canyon'
                }
        },
        {
            'url':
                {
                    'S':'https://i.natgeofe.com/n/40c8ec76-10ca-483d-9a78-aa7d75e12150/zion-national-park-canyon-junction-bridge.jpg?w=636&h=424'
                },
            'location':
                {
                    'S':'Utah'
                },
            'park':
                {
                    'S':'Zion'
                }
        },
        {
            'url':
                {
                    'S':'https://i.natgeofe.com/n/ceadd788-b858-428c-998d-f3d33cb4644b/great-smoky-mountains-national-park-mountains-sunset.jpg?w=636&h=306'
                },
            'location':
                {
                    'S':'Tennessee'
                },
            'park':
                {
                    'S':'Great Smoky Mountains'
                }
        },
        {
            'url':
                {
                    'S':'https://i.natgeofe.com/n/f396d91c-9723-4d65-a9ab-b536fc340864/death-valley-national-park-jonathan-irish.jpg?w=636&h=424'
                },
            'location':
                {
                    'S':'California'
                },
            'park':
                {
                    'S':'Death Valley'
                }
        },
        {
            'url':
                {
                    'S':'https://i.natgeofe.com/n/5b1a4b25-48a9-4dbd-bdd3-3d4240871d8e/redwood-national-park-sunrise-forest.jpg?w=636&h=424'
                },
            'location':
                {
                    'S':'California'
                },
            'park':
                {
                    'S':'Redwood'
                }
        },
        {
            'url':
                {
                    'S':'https://i.natgeofe.com/n/85b193ea-6938-44c7-9e13-f2e5ac3bcdf7/grand-teton-national-park-schwabachers-landing.jpg?w=636&h=424'
                },
            'location':
                {
                    'S':'Wyoming'
                },
            'park':
                {
                    'S':'Grand Teton'
                }
        }
    ]
}


def unique_states(parks):
    unique_parks = []
    for park in parks:
        if park['location']['S'] not in unique_parks:
            unique_parks.append(park['location']['S'])
    return unique_parks


@app.route('/')
def index():
    return render_template('index.html', parks=parks['Items'], unique_parks=unique_states(parks['Items']))















