from flask import Flask, render_template
from boto3.dynamodb.conditions import Key
import boto3
import os
import json


app = Flask(__name__)

session = boto3.Session()
ddb = session.client(service_name='dynamodb')

ddb_table='is-531-project-3-dynamodb-table-12345'

parks = ddb.scan(TableName=ddb_table)


def unique_states(parks):
    unique_parks = []
    for park in parks:
        if park['location']['S'] not in unique_parks:
            unique_parks.append(park['location']['S'])
    return unique_parks


@app.route('/')
def index():
    return render_template('index.html', parks=parks['Items'], unique_parks=unique_states(parks['Items']))

