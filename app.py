#!/usr/bin/env python3
import os

from aws_cdk import core as cdk
from aws_cdk import core

from is531_project3.is531_project3_stack import Is531Project3Stack


app = core.App()
Is531Project3Stack(app, "Is531Project3Stack")

app.synth()
