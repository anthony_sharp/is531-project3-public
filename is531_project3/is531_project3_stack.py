from aws_cdk import (
    core as cdk,
    aws_ecr as ecr,
    aws_ecs as ecs,
    aws_ec2 as ec2,
    aws_logs as logs,
    aws_elasticloadbalancingv2 as elbv2,
    aws_s3 as s3,
    aws_dynamodb as ddb,
    aws_s3_deployment as s3_deployment
)

class Is531Project3Stack(cdk.Stack):

    def __init__(self, scope=cdk.Construct, construct_id=str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)


        vpc = ec2.Vpc(self, "TheVPC",
            cidr="10.0.0.0/16"
        )


        ### S3 ###
        static_assets_bucket = s3.Bucket(self, 'static_assets',
            auto_delete_objects=True,
            removal_policy=cdk.RemovalPolicy.DESTROY,
            public_read_access=True,
            bucket_name='is-531-project-3-static-assets-123456'
        )

        s3_deployment.BucketDeployment(self, 'national-park-img-deployment',
            destination_bucket=static_assets_bucket,
            sources=[s3_deployment.Source.asset('./imgs/national_parks.zip')]
        )

        ### IAM ###
        ecsSG = ec2.SecurityGroup(self, "Proejct3-SG-ECS",
            allow_all_outbound=True,
            vpc=vpc
        )


        ### ECR ###
        repository = ecr.Repository.from_repository_name(self, "proj3-repo", 
            "is531-proj3",
        )


        ### FARGATE ###
        cluster = ecs.Cluster(self, 'Project3-Cluster',
            vpc=vpc,
            cluster_name='is-531-project-3-cluster-12345'
        )

        taskDef = ecs.FargateTaskDefinition(self, 'Project3-TaskDefinition',
            memory_limit_mib=512,
            cpu=256,
        )

        container = taskDef.add_container('Project3-Container', 
            container_name='project3',
            image=ecs.ContainerImage.from_ecr_repository(repository),
            memory_limit_mib=512,
            cpu=256,
        )

        container.add_port_mappings(
            ecs.PortMapping(
                container_port=80,
                host_port=80,
                protocol=ecs.Protocol.TCP,
            )
        )

        service = ecs.FargateService(self, 'Project3-Fargate-Service',
            cluster=cluster,
            task_definition=taskDef,
            desired_count=1,
            security_groups=[ecsSG],
        )


        ### LOAD BALANCER ###
        alb = elbv2.ApplicationLoadBalancer(self, 'Project3-ALB',
            vpc=vpc,
            vpc_subnets=ec2.SubnetSelection(subnets=vpc.public_subnets),
            internet_facing=True,
        )

        albSG = ec2.SecurityGroup(self, "Project3-ALB-SG",
            vpc=vpc,
            allow_all_outbound=True,
        )

        albSG.add_ingress_rule(
            ec2.Peer.any_ipv4(),
            ec2.Port.tcp(80),
            "Allow http traffic"
        )

        alb.add_security_group(albSG)

        # allow HTTP connections
        listener = alb.add_listener('Project3-ALB-Listener', 
            open=False,
            port=80,
        )

        # attach ALB to ECS service
        listener.add_targets('Project3-Targets', 
            port=80,
            targets=[service.load_balancer_target(
                container_name=container.container_name,
                container_port=80,
                protocol=ecs.Protocol.TCP,
            )],
            # targets=[service],
            health_check=elbv2.HealthCheck(
                interval=cdk.Duration.minutes(1),
                path='/health',
                timeout=cdk.Duration.seconds(5),
            )
        )

        ### DYNAMODB ###
        dynamoTable = ddb.Table(self, "project3-table",
            partition_key=ddb.Attribute(
                name="park", 
                type=ddb.AttributeType.STRING    
            ),
            table_name='is-531-project-3-dynamodb-table-12345',
        )
        
        dynamoTable.apply_removal_policy(cdk.RemovalPolicy.DESTROY)
        dynamoTable.grant_read_data(service.task_definition.task_role)
        dynamoTable.grant_read_data(taskDef.execution_role)
        dynamoTable.grant_full_access(service.task_definition.task_role)
        dynamoTable.grant_full_access(taskDef.execution_role)

        ### CDK OUTPUT ###
        cdk.CfnOutput(self, 'Project3-ALB-DNS', value=f'http://{alb.load_balancer_dns_name}', )

