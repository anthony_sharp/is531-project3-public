#!/bin/bash

# Set ACCOUNT_ID env var with export ACCOUNT_ID=123456789
ECR_REPO = is531-proj3
REGION = us-east-1
CLUSTER = is-531-project-3-cluster-12345

# Update ecr repo then cdk application 
deploy: get-accout-id ecr-update cdk 

# Deploys ecr repo then cdk application 
deploy-init: get-accout-id deploy-ecr cdk
	@echo "\n================================================"
	@echo "POPULATING DYNAMODB TABLE..."
	@echo "================================================\n"
	python3 utils/write_dynamo.py
	@echo "\n================================================"
	@echo "RESTARTING ECS TASK..."
	@echo "================================================\n"
	@$(eval TASK_ARN=$(shell aws ecs list-tasks --cluster $(CLUSTER) | jq .taskArns[0] | tr -d '"'))
	@aws ecs stop-task --cluster $(CLUSTER) --task $(TASK_ARN)

# Pushes ecr repo from Dockerfile
ecr-update: ecr-push
ecr-build:
	@echo "\n================================================"
	@echo "BUILDING DOCKER IMAGE..."
	@echo "================================================\n"
	docker build -t $(ECR_REPO) .

ecr-login: ecr-build
	@echo "\n================================================"
	@echo "GETTING ECR PASSWORD AND LOGGING IN TO DOCKER..."
	@echo "================================================\n"
	aws ecr get-login-password --region $(REGION) | docker login --username AWS --password-stdin $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com

ecr-tag-image: ecr-login
	@echo "\n================================================"
	@echo "TAGGING LATEST DOCKER IMAGE"
	@echo "================================================\n"
	docker tag $(ECR_REPO):latest $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(ECR_REPO):latest

ecr-push: ecr-tag-image
	@echo "\n================================================"
	@echo "PUSHING DOCKER IMAGE TO ECR REPO..."
	@echo "================================================\n"
	docker push $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(ECR_REPO):latest

# Deploys ecr repo (must run first)
deploy-ecr: ecr-tag-image
	@echo "\n================================================"
	@echo "CREATING ECR REPO..."
	@echo "================================================\n"
	aws ecr create-repository \
    --repository-name $(ECR_REPO) \
    --image-scanning-configuration scanOnPush=true \
    --region $(REGION)
	@echo "\n================================================"
	@echo "PUSHING DOCKER IMAGE TO ECR REPO..."
	@echo "================================================\n"
	docker push $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(ECR_REPO):latest

# Deploys cdk app
cdk:
	@echo "\n================================================"
	@echo "DEPLOYING CDK APP..."
	@echo "================================================\n"
	cdk deploy --require-approval never

get-accout-id:
	@echo "\n================================================"
	@echo "SETTING ACCOUNT_ID ENV VAR..."
	@echo "================================================\n"
	$(eval ACCOUNT_ID=$(shell aws sts get-caller-identity | jq '.Account' | tr -d '"'))

# Destroys all resources
destroy-all: ecr-destroy cdk-destroy

ecr-destroy:
	@echo "\n================================================"
	@echo "DELETING ECR IMAGES..."
	@echo "================================================\n"
	aws ecr batch-delete-image \
	--repository-name $(ECR_REPO) \
	--image-ids imageTag=latest

	@echo "\n================================================"
	@echo "DESTROYING ECR REPO..."
	@echo "================================================\n"
	aws ecr delete-repository \
    --repository-name $(ECR_REPO)

cdk-destroy:
	@echo "\n================================================"
	@echo "DESTROYING CDK APPLICATION..."
	@echo "================================================\n"
	yes | cdk destroy


	
	
	