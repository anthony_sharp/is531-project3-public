FROM python

COPY ./requirements-flask.txt /app/requirements.txt

WORKDIR /app

# # Use for local testing
# ENV AWS_ACCESS_KEY_ID=
# ENV AWS_SECRET_ACCESS_KEY=
# ENV AWS_DEFAULT_REGION=

RUN pip install -r requirements.txt

COPY ./flask_app /app

EXPOSE 80

CMD ["flask", "run", "--port=80", "--host=0.0.0.0"]